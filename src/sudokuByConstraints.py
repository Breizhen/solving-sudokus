from pycsp3 import *
import argparse
from main import load_sudoku

def sudokuByConstraints(board):
    clear()
    # cell [ i ][ j ] is the value at row i and col j
    cell = VarArray(size =[9, 9], dom = range (1, 10))
    
    # imposing distinct values on each row and each column
    satisfy (AllDifferent(cell , matrix = True))
    
    # imposing distinct values on each block tag ( blocks )
    satisfy(AllDifferent (cell[i:i+3, j:j+3]) for i in [0, 3, 6] for j in [0, 3, 6])
    
    # imposing clues tag ( clues )
    satisfy(cell[i,j] == board[i][j] for i in range(9) for j in range(9) if (board & board[i][j] > 0).all())

    if solve(solver=CHOCO) == SAT :
        print("solution: ", values(cell))
    else :
        print("Pas de solution")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', default=os.path.join('sudokus', 'sudokus.txt'), help='path to the sudoku')
    parser.add_argument('--num', default=0, type=int, help='sudoku number')
    args = parser.parse_args()
    options = vars(args)  # convert to ordinary dict
    board = load_sudoku(options['path'], options['num'])
    sudokuByConstraints(board)