import argparse
import random as rd
from pycsp3 import *
from main import load_sudoku, solve


def solvedBoard():
    
    cell = VarArray(size=[9,9], dom=range(1,10))
    
    # Rows
    for row in range(9):
        satisfy( AllDifferent(cell[row, j] for j in range(9)) )
    #end for
    
    # Columns
    for col in range(9):
        satisfy( AllDifferent(cell[i, col] for i in range(9)) )
    #end for
    
    # Blocks
    for (row, col) in [(i,j) for i in range(3) for j in range(3)]:
        satisfy( AllDifferent(cell[3*row+i, 3*col+j] for i in range(3) for j in range(3)) )
    #end for
    
    if solve(solver=CHOCO, sols=10000) == SAT:

        f = open('./solvedGrids.txt', 'w')
        
        for indSol in range(10000):
            for i in range(9):
                for j in range(9):
                    f.write(str(value(cell[i][j], sol=indSol)))
            f.write(',\n')        
        f.close()
    #end if  
#end def



def genPuzzle(diff, missingCells):
    board = load_sudoku('solvedGrids.txt', rd.randint(0,9999))

    
    delCells = []
    currentDiff = 0
    cellCount = 0
    
    while (currentDiff < diff) | (cellCount < missingCells):    
        for ind in delCells:
            board[ind[0]][ind[1]] = 0
        #end for
        
        i = rd.randrange(9)
        j = rd.randrange(9)
        
        if board[i][j] != 0:
            
            delCells.append((i,j))
            
            val = board[i][j]
            board[i][j] = 0
            cellCount += 1
            
            
            
            currentDiff = solve(board, difficulty=True)['difficulty'][0]
            if currentDiff == -1:
                board[i][j] = val
                delCells.pop()
                cellCount -= 1
            #end if
        #end if
        
    #end while
    
    for ind in delCells:
        board[ind[0]][ind[1]] = 0
    #end for
    
    
    return board
#end def


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--diff', type=float, help='minimum difficulty for the problem')
    parser.add_argument('--missCell', type=int, help='sudoku number')
    args = parser.parse_args()
    options = vars(args)  # convert to ordinary dict
    board = genPuzzle(options['diff'], options['missCell'])
    print(board)
    