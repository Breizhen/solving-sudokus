import os
import time
import argparse
import numpy as np
from sudoku_alg import print_board
from methode.methode_1 import method_1
from methode.methode_2 import method_2
from methode.methode_3 import method_3
from methode.methode_4 import method_4
from methode.methode_5 import method_5

def load_sudoku(filename, n=0):
    """
    Retrieves a sudoku from the file 'filename' and takes the n-th one if there are several
    """
    try:
        fic = open(filename, 'r')
    except IOError:
        raise BaseException("the file :", filename, " was not found")

    i = 0
    for line in fic:
        if i == n:
            return np.array([[int(line[i + j * 9]) for i in range(9)] for j in range(9)])
        i += 1
        
    raise("problem opening the sudoku number ", n)

 
def first_check(board):
    return [[[k for k in range(1, 10)] if board[i][j]==0 else board[i][j] for j in range(9)] for i in range(9)]


def no_problem(board):
    """
    Check if the sudoku is valid (if each number appears at most once in each row, column, or box)
    """
    for k in range(1, 10):
        for i in range(9):
            if occurance(board[i], k) > 1:
                return False
        for j in range(9):
            if occurance(board[:, j], k) > 1:
                return False
        for b in range(9):
            if occurance([board[3*(b//3)+bi//3][3*(b%3)+bi%3] for bi in range(9)], k) > 1:
                return False
    return True

def occurance(l, x):
    occ = 0
    for y in l:
        if x == y:
            occ += 1
    return occ    


def solve(board, difficulty=False, benchmark=False, affiche_board=False, verif=False, affiche_check=False, affiche_last_sudoku=False):
    """
    Solves the Sudoku puzzle.
    difficulty: calculates the difficulty of the Sudoku puzzle.
    benchmark: measures the time it takes to solve the Sudoku puzzle.
    affiche_board: displays the Sudoku puzzle at each step of the resolution.
    verif: verifies if the final Sudoku puzzle is correct.
    affiche_check: displays check_i: number of uses of the method i.
    affiche_last_sudoku: displays the solved Sudoku puzzle.
    """
    counts = [0, 0, 0, 0]
    t0 = time.time()

    current = 0
    possibilities = [first_check(board)]
    board = [board]
    methode5 = 0    # number of times method 5 has been used (number of disjunctions made)
    sudoku_have_problem = False
    MAX_LOOP = 50

    while not(verif_board(board[current])) or current > MAX_LOOP:

        # Method 1
        board[current], possibilities[current], r = method_1(board[current], possibilities[current])
        if affiche_check:
            print('check_1:', r)
        counts[0] += r

        if r == 0:
            # Method 2
            board[current], possibilities[current], r = method_2(board[current], possibilities[current])
            if affiche_check:
                print('check_2:', r)
            counts[1] += r

            if r == 0:
                # Method 3
                board[current], possibilities[current], r = method_3(board[current], possibilities[current])
                if affiche_check:
                    print('check_3:', r)
                counts[2] += r
               
                if r == 0:
                    # Method 4
                    board[current], possibilities[current], r = method_4(board[current], possibilities[current])
                    if affiche_check:
                        print('check_4:', r)
                    counts[3] += r
        
        if affiche_board:
            print('')
            print_board(board[current])

        if methode5 != 0 and not(no_problem(board[current])):
            sudoku_have_problem = True
            
        if r == 0 or sudoku_have_problem:
            # Méthode 5
            if affiche_check:
                print('\033[94m')   # start blue color
                print('- - - METHOD 5 - - -')
            
            if sudoku_have_problem:
                if affiche_check:
                    print('problem in sudoku -> next case')
                current += 1
                sudoku_have_problem = False
            
            elif r == 0:
                if affiche_check:
                    print('we make a disjunction')
                aws = method_5(board[current], possibilities[current], affiche_check)

                if aws == 'go_next':
                    if affiche_check:
                        print('problem in sudoku -> next case')
                    current += 1
                    sudoku_have_problem = False
                
                else:
                    new_board, new_possibilities = aws
                    board += new_board
                    possibilities += new_possibilities
                    current += 1
                    methode5 += 1

            else:
                print('ERROR')
                print('\033[0m')    # end blue color
                return {'ERROR': True}


            if current == len(board):
                # print('ERROR: no sudoku is possible')
                print('\033[0m')    # end blue color
                return {'ERROR': True}
            
            if affiche_check:
                print('\033[0m')    # end blue color
            
    t1 = time.time()
    result = {'ERROR': False}
    if current == MAX_LOOP:
        result['ERROR'] = True

    if affiche_last_sudoku:
        print('')
        print_board(board[current])

    if difficulty:
        w = np.array([1, 3, 9, 27])
        complexity = (sum(w * counts) + 81 * methode5) / (sum(counts) + methode5)
        result['difficulty'] = (complexity, methode5)
      
    if benchmark:
        result['time'] = t1 - t0
    
    if verif:
        result['verif'] = verif_board(board[current])
        
    return result


def verif_board(board):
    """
    check if the sudoku is completed and has no problems
    """
    if 0 in board.flatten():
        return False
    
    for k in range(1, 10):
        for i in range(9):
            if not(k in board[i]):
                return False
        for j in range(9):
            if not(k in board[:][j]):
                return False
        for b in range(9):
            if not(k in [board[3*(b//3)+bi//3][3*(b%3)+bi%3] for bi in range(9)]):
                return False
    return True


def print_result(result):
    if result['ERROR']:
        print('an error occurred in the sudoku')
    if 'verif' in result:
        print('complete sudoku        :', result['verif'])
    if 'time' in result:
        print("Time                   : %.3fs" %result['time'])
    if 'difficulty' in result:
        (complexity, methode5) = result['difficulty']
        print("estimated difficulty   : %.2f" %complexity)
        print("number of disjunctions :", methode5)
        

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', default=os.path.join('sudokus', 'sudokus.txt'), help='path to the sudoku')
    parser.add_argument('--num', default=0, type=int, help='sudoku number')
    parser.add_argument('--diff', default=True, type=bool, help='calculates the difficulty of the Sudoku puzzle')
    parser.add_argument('--time', default=True, type=bool, help='measures the time it takes to solve the Sudoku puzzle')
    parser.add_argument('--verif', default=False, type=bool, help='verifies if the final Sudoku puzzle is correct')
    parser.add_argument('--print_board', default=False, type=bool, help='displays the Sudoku puzzle at each step of the resolution')
    parser.add_argument('--last_board', default=False, type=bool, help='displays the solved Sudoku puzzle')
    parser.add_argument('--print_check', default=False, type=bool, help='displays check_i: number of uses of the method i')
    args = parser.parse_args()
    options = vars(args)  # convert to ordinary dict
    board = load_sudoku(options['path'], options['num'])
    result = solve(board, 
                   difficulty=options['diff'], 
                   benchmark=options['time'], 
                   affiche_board=options['print_board'], 
                   verif=options['verif'], 
                   affiche_check=options['print_check'], 
                   affiche_last_sudoku=options['last_board'])
    print_result(result)