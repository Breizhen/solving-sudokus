import io, time, numpy as np, matplotlib.pyplot as plt, pandas as pd, pygame, randdist, random
from scipy.optimize import curve_fit
from copy import deepcopy
from main import load_sudoku, solve
from puzzleCreator import genPuzzle
from sudokuByConstraints import sudokuByConstraints
from main import Board


def toString(board):
    string = ""
    for i in range(9):
        for j in range(9):
            string += str(board[i][j])
    string += ","
    return string
#end def

def findIndex(keys, value):
    it = len(keys)-1
    found = False
    while it > 0 and not found:
        if value >= keys[it]:
            found = True
        else:
            it -= 1
    return keys[it]

def sortByDiff(path):
    diff2ind = {1: 0, 1.5: 1, 2: 2, 2.5: 3, 3: 4, 3.5: 5, 4: 6}
    
    err = []
    with open('Code_Prof/sudokus/exepts.txt') as f:
        for line in f:
            err.append(int(line))
    
    dirs = [open(path + "/sorted_{}-{}.txt".format(i, i+0.5), "w") for i in np.arange(1, 4.5, 0.5)]
    
    err_ind = 0
    for it in range(9999):
        if it == err[err_ind] and err_ind != (len(err)-1):
            err_ind+=1
        else:
            print("\n---- HERE (it = {}) ----\n".format(it))
            board = load_sudoku(path+"/sudokus.txt", it)
            board_save = deepcopy(board)
            diff = solve(board, difficulty=True)['difficulty'][0]
            
            ind = findIndex(list(diff2ind.keys()), diff)
            dirs[diff2ind[ind]].write(toString(board_save)+"\n")
        
    map(io.TextIOWrapper.close, dirs)
#end def

    
def computeTime_det(path="Code_Prof/sudokus/"):
    with open(path + "times.csv", "w") as file:
        for i in np.arange(1, 4.5, 0.5):
            file.write("sorted_{}-{},".format(i, i+0.5))
            with open(path+"sorted_{}-{}.txt".format(i, i+0.5)) as sort:
                nbLines = len(sort.readlines())
            for line in range(nbLines):
                board = load_sudoku(path+"sorted_{}-{}.txt".format(i, i+0.5), line)
                time = solve(board, benchmark=True)['time']
                file.write("{},".format(time))
            file.write("\n")
       
            
def computeTime_solver(path="Code_Prof/sudokus/"):
    with open(path + "times_solv.csv", "w") as file:
        for i in np.arange(1, 4.5, 0.5):
            file.write("sorted_{}-{},".format(i, i+0.5))
            with open(path+"sorted_{}-{}.txt".format(i, i+0.5)) as sort:
                nbLines = len(sort.readlines())
            for line in range(766):
                print("\nline = ", line,"\n")
                board = load_sudoku(path+"sorted_{}-{}.txt".format(i, i+0.5), line)
                t0 = time.time()
                print("\nt0 = ", t0, "\n")
                sudokuByConstraints(board)
                t1 = time.time()
                elapsed = t1 - t0
                file.write("{},".format(elapsed))
            file.write("\n")

            
def computeTime_search(path="Code_Prof/sudokus/"):
    with open(path + "times_search.csv", "w") as file:
        for i in range(1,8):
            file.write("times_{}".format(i)+("," if i < 7 else "\n"))
        for rep in range(1,11):
            for diff in range(1,8):
                times = path+"Gen_x_Search/sud_{}_{}.txt".format(diff, rep)
                screen = pygame.display.set_mode((540, 590))
                screen.fill((255, 255, 255))
                board = Board(screen, times)
                t0 = time.time()
                solv = SearchSolver(board,t0)
                solv.visualSolve(0)
                t1 = time.time()
                elapsed = t1 - t0
                file.write("{}".format(elapsed)+("," if diff < 7 else "\n"))


def computeTime_rd_det(path="Puzzle_Creator/"):
    with open(path + "times.csv", "w") as file:
        file.write("diff,time\n")
        for i in range(500):
            diff = random.uniform(7.0,10.0)
            missCells = randdist.randint(min_value=5, max_value=40, formula=lambda x:-4*x**2/175 + 32*x/35 + 1, sample_size=1)
            
            board = genPuzzle(diff, missCells)
            print(board)
            sol = solve(board=board, difficulty=True, benchmark=True)
            diff = sol['difficulty'][0]
            time = sol['time']
            
            file.write("{},{}\n".format(diff, time))

            

 
           
            
def plotting(path):
    
    with open(path, newline='') as file:
        
        data = pd.read_csv(file)
        x = data['diff'].tolist()
        y = data['time'].tolist()
        
        """
            1.5, 2.0, 2.5, ..., 11
            1.5 + 0.5*i, i = 0..19
            => i = (val - 1.5)*2
        """
        
        sorting = [[] for _ in range(20)]
        
        for i in range(len(x)):
            roundup = round(2*x[i])/2
            ind = int((roundup - 1.5)*2)
            sorting[ind].append(y[i])
            
        med = [np.median(sorting[ind]) for ind in range(20)]
        q1 = [np.quantile(sorting[ind], 0.25) for ind in range(20)]
        q3 = [np.quantile(sorting[ind], 0.75) for ind in range(20)]
        d1 = [np.quantile(sorting[ind], 0.1) for ind in range(20)]
        d9 = [np.quantile(sorting[ind], 0.9) for ind in range(20)]
        
        
        popt, pcov = curve_fit(lambda t, a, b, c: a * np.exp(b * t) + c, [1.5+0.5*i for i in range(20)], med)
        a = popt[0]
        print("a = ", a)
        b = popt[1]
        print("b = ", b)
        c = popt[2]
        print("c = ", c)
        med_x_fitted = np.linspace(np.min(x), np.max(x), 100)
        med_y_fitted = a * np.exp(b * med_x_fitted) + c
        
        plot = plt.scatter(x,y, c="lightskyblue", label="Data")
        plt.xticks(np.arange(1.5, 11.5, step=0.5))
        plt.xlabel("Difficulty")
        plt.ylabel("Time (s)")
        plt.title("Runtime according to puzzle difficulty on randomly generated puzzles")
        
        plt.plot([1.5+0.5*i for i in range(20)], d1, '-ok', c="yellow", markersize=6, markerfacecolor="gold", markeredgecolor="gold",label="Deciles")
        
        plt.plot([1.5+0.5*i for i in range(20)], d9, '-ok', c="yellow", markersize=6, markerfacecolor="gold", markeredgecolor="gold")
        
        plt.fill_between([1.5+0.5*i for i in range(20)], d9, d1, alpha=0.2, color="yellow")
        
        
        plt.plot([1.5+0.5*i for i in range(20)], q1, '-ok', c="orange", markersize=6, markerfacecolor="darkorange", markeredgecolor="darkorange",label="Quartiles")
        
        plt.plot([1.5+0.5*i for i in range(20)], q3, '-ok', c="orange", markersize=6, markerfacecolor="darkorange", markeredgecolor="darkorange")
        
        plt.fill_between([1.5+0.5*i for i in range(20)], q3, q1, alpha=0.2, color="gold")
        
        
        plt.plot([1.5+0.5*i for i in range(20)], med, '-ok', c="pink", markersize=6, markerfacecolor="red", markeredgecolor="red" ,label="Median")
        
        plt.plot(med_x_fitted, med_y_fitted, 'k', label='Median fitting', c='red')
        
        plt.legend()
        plt.show()
        
        
