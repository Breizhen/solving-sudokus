from sudoku_alg import valid

def intersection(l1, l2):
    """
    Intersects 2 lists
    """
    return [x for x in l1 if x in l2]


def method_1(board, possibilities):
    """
    Updates the possibilities (with method 1) and returns the number of times method 1 was used
    """
    count = 0
    for i in range(9):
        for j in range(9):
            if board[i][j] == 0:
                p_ij = [p for p in range(1, 10) if valid(board, (i, j), p)]
                possibilities[i][j] = intersection(p_ij, possibilities[i][j])
                if len(possibilities[i][j]) == 1:
                    board[i][j] = possibilities[i][j][0]
                    possibilities[i][j] = possibilities[i][j][0]
                    count += 1   
    return board, possibilities, count