def same_line_colonne(x, box):
    """
    looks at whether x (int) is only likely to be in a single row or column of a box ([int])
    returns an integer tuple:
    - the row number if x is only likely in one row, otherwise returns -1
    - the number of the column if x is only probable in a single column, otherwise returns -1
    """
    if x in box:    # If x is in the box, then we don't need to do this method
        return -1, -1
    
    lx = None
    cx = None

    for i, p in enumerate(box):
        if type(p) == list and x in p:

            if lx is None:  # If this is the first time x is seen (if Lx=None then cx=None)
                lx = i//3
                cx = i % 3

            if lx != i//3:  # If lx != i//3 then x has already been seen in another line, then -1 is returned for the rows
                lx = -1

            if cx != i % 3: # If cx != i%3 then x has already been seen in another column, then -1 is returned for the columns
                cx = -1
    
    if lx is None:
        lx, cx = -1, -1
    
    return lx, cx
    

def method_3(board, possibilities):
    """
    Execute method 3.1 in all sukodu
    """
    count = 0

    # We loop through the boxes
    for b in range(9):
        p_box = [possibilities[3*(b//3) + bi//3][3*(b%3) + bi%3] for bi in range(9)]
        for x in range(9):
            lx, cx = same_line_colonne(x, p_box)
            change = False

            # check the rows
            if lx != -1:
                line = 3*(b//3) + lx
                colonne = [i for i in range(9) if i<3*(b%3) or 3*(b%3)+2<i] # list of indices of columns that are not in the box
                for c in colonne:
                    if type(possibilities[line][c]) == list and x in possibilities[line][c]:
                        possibilities[line][c].pop(possibilities[line][c].index(x))
                        change = True
                if change:
                    count += 1
            
            # check the colunms
            if cx != -1:
                colonne = 3*(b%3) + cx
                line = [i for i in range(9) if i<3*(b//3) or 3*(b//3)+2<i] # list of indices of rows that are not in the box
                for l in line:
                    if type(possibilities[l][colonne]) == list and x in possibilities[l][colonne]:
                        possibilities[l][colonne].pop(possibilities[l][colonne].index(x))
                        change = True
                if change:
                    count += 1
        
        if change:
            count += 1

    return board, possibilities, count