# Méthod 3.2 et 4


def line_box_completed(board, i, b):
    """
    tells if in the box b (number between [0, 2]), the line i is complete
    returns 1 if true, 0 otherwise
    """
    if board[i, b * 3] != 0 and board[i, b * 3 + 1] != 0 and board[i, b * 3 + 2] != 0:
        return 1
    return 0


def colonne_box_completed(board, j, b):
    """
    tells if in the box b (number between [0, 2]), the column j is complete
    returns 1 if true, 0 otherwise
    """
    if board[b * 3, j] != 0 and board[b * 3 + 1, j] != 0 and board[b * 3 + 2, j] != 0:
        return 1
    return 0


def remove(a, b):
    """
    sends a to all elements of b
    returns new_a, and a boolean telling whether or not a has been modified
    """
    n = len(a)
    new_a = list(filter(lambda x: not(x in b), a))
    return new_a, len(new_a) != n


def check_line_i(board, possibilities, i):
    """
    performs method 4 on line i
    returns possibilities (modified) and a boolean which will be True if possibilities has been modified
    """
    completed_line = [line_box_completed(board, i, b) for b in range(3)]

    if sum(completed_line) != 2:    # if != 2, then the method does not apply
        return possibilities, False
    
    b = completed_line.index(0) # index of the box that is not complete on line i

    box_indexes = [(3 * (i // 3) + j % 3, b * 3 + j // 3) for j in range(9)]            # indexes of all boxes in box b
    line_i_value = list(filter(lambda x: x != 0, board[i, :]))                          # number already found in line i
    not_in_line_i = list(filter(lambda x: x not in line_i_value, list(range(1, 10))))   # number not yet found in column j

    modif = False
    for i1, j1 in box_indexes:
        if board[i1, j1] == 0 and i1 != i:
            # if (i1, j1) has not been found and i1 != i, we remove not_in_line_i from possibilities[i1, j1]
            possibilities[i1][j1], change = remove(possibilities[i1][j1], not_in_line_i)
            modif = modif or change
    
    return possibilities, modif


def check_colonne_j(board, possibilities, j):
    """
    performs method 4 on line i
    returns possibilities (modified) and a boolean which will be True if possibilities has been modified
    """
    completed_col = [colonne_box_completed(board, j, b) for b in range(3)]

    if sum(completed_col) != 2:    # if != 2, then the method does not apply
        return possibilities, False
    
    b = completed_col.index(0) # index of the box that is not complete on column j

    box_indexes = [(b * 3 + i // 3, 3 * (j // 3) + i % 3) for i in range(9)]        # indexes of all boxes in box b
    col_j_value = list(filter(lambda x: x != 0, board[:, j]))                       # number already found in column j
    not_in_col_j = list(filter(lambda x: x not in col_j_value, list(range(1, 10)))) # number not yet found in column j

    modif = False
    for i1, j1 in box_indexes:
        if board[i1, j1] == 0 and j1 != j:
            # if (i1, j1) is not found and j1 != j, remove not_in_col_j from possibilities[i1, j1]
            possibilities[i1][j1], change = remove(possibilities[i1][j1], not_in_col_j)
            modif = modif or change
    
    return possibilities, modif
    

def method_4(board, possibilities):
    count = 0
    for i in range(9):
        possibilities, modif = check_line_i(board, possibilities, i)
        if modif:
            count += 1
    for j in range(9):
        possibilities, modif = check_colonne_j(board, possibilities, j)
        if modif:
            count += 1
    return board, possibilities, count
