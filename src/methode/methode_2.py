def occurences_sublist(x, L):
    """
    Counts the number of occurrences of x in the SUBLISTS of L and an index of occurrence of x in L
    If x is in L, then returns 0, -1
    """
    if x in L:
        return 0, -1
    
    count, k = 0, -1
    for i, l in enumerate(L):
        if type(l) == list and x in l:
            count += 1
            k = i
    return count, k


def method_2(board, possibilities):
    """
    Execute method 2 on the whole sudoku
    """
    count = 0

    # We loop through the rows
    for i in range(9):
        p_line = [possibilities[i][j] for j in range(9)]
        for k in range(9):
            c, j = occurences_sublist(k, p_line)

            if c == 1:  # If there is only one possibility of k in the row
                board[i][j] = k
                possibilities[i][j] = k
                count += 1
    
    # We loop through the column
    for j in range(9):
        p_col = [possibilities[i][j] for i in range(9)]
        for k in range(9):
            c, i = occurences_sublist(k, p_col)

            if c == 1:   ## If there is only one possibility of k in the column
                board[i][j] = k
                possibilities[i][j] = k
                count += 1
    
    # We loop through the boxes
    for b in range(9):
        p_box = [possibilities[3*(b//3)+bi//3][3*(b%3)+bi%3] for bi in range(9)]
        for k in range(9):
            c, bi = occurences_sublist(k, p_box)

            if c == 1:   # If there is only one possibility of k in the box
                i, j = 3*(b//3) + bi//3, 3*(b%3) + bi%3 
                board[i][j] = k
                possibilities[i][j] = k
                count += 1
    
    return board, possibilities, count