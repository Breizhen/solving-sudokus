# Method 5
# If the 4 methods have failed, we take the empty cell that has the least possibilities,
# We then choose a possibility, then continue solving the sudoku.
# If we arrive at an inconsistency, then it would mean that it was not the right possibility

import numpy as np


def get_cell_with_less_possibilities(board, possibilities):
    nb_possibilities = 10
    i0, j0 = -1, -1
    for i in range(9):
        for j in range(9):
            if board[i][j] == 0 and type(possibilities[i][j]) == list:
                nb_poss = len(possibilities[i][j])
                if nb_poss == 2:
                    return i, j
                if nb_poss > 2 and nb_poss < nb_possibilities:
                    nb_poss = nb_possibilities
                    i0, j0 = i, j
    return i0, j0


def copy_list(l):
    if type(l) == list:
        l_copy = []
        for x in l:
            l_copy.append(copy_list(x))
        return l_copy
    return l

def copy_board(board):
    return np.copy(board)


def copy_possibilities(possibilities):
    possibilities_copy = [[[] for j in range(9)] for i in range(9)]
    for i in range(9):
        for j in range(9):
            if type(possibilities[i][j]) == list:
                for p in possibilities[i][j]:
                    possibilities_copy[i][j].append(p)
    return possibilities_copy


def method_5(board, possibilities, affiche_check):
    i, j = get_cell_with_less_possibilities(board, possibilities)

    if (i, j) == (-1, -1):
        # this sudoku is not possible, go next
        return 'go_next'
    
    if affiche_check:
        print('case disjunction in the box:', i, j, '-> possibilities:', possibilities[i][j])
    
    all_board, all_possibilities = [], []

    for p in possibilities[i][j]:
        board_copy = copy_board(board)
        board_copy[i][j] = p
        possibilities_copy = copy_possibilities(possibilities)
        all_board.append(board_copy)
        all_possibilities.append(possibilities_copy)

    return all_board, all_possibilities

