# Sudodoku game - Dorian Lauwerier, Benjamin Oberthür, Adrien Zabban

## Content

- `./Report.pdf`: Explanations of the project, implementation with analysis of the results
- `./src`:
  - `/sudokus`: Folder containing sudoku instances proposed in [Hugue Bersini's Git repository](https://github.com/iridia-ulb/AI-book)
  - `/main.py` + `/methode`: Pseudo-deterministic algorithm solving the sudoku instances. Using `/sudoku_alg.py`, also provided in Hugue Bersini's Git repository.
  - `/sudokuByConstraints.py`: functions solving, from an instance file, a sudoku puzzle with the constraint programming solver
  - `/benchmark.py`: functions used for benchmarking the different algorithms
- `./data`: Different CSV files containing the data used for plotting the different performances.

***

## Execution

To execute properly the functions, place the current directory of your terminal in the `src` folder.

- `main.py`: The main function takes a set of arguments with different functionalities:

|    Argument     | Type    |     Default value     |                              Use                              |
|-----------------|---------|-----------------------|---------------------------------------------------------------|
| `--path`        | String  | "sudokus/sudokus.txt" | Path of the instance to solve                                 |
| `--num`         | Integer | 0                     | Number of the instance chosen in the file                     |
| `--diff`        | Boolean | True                  | Returns or not the difficulty associated with the instance    |
| `--time`        | Boolean | True                  | Returns or not the time spent to solve the instance           |
| `--verif`       | Boolean | False                 | Verifies or not if the final board is correct                 |
| `--print_board` | Boolean | False                 | Displays or not the board at each step of the solving         |
| `--last_board`  | Boolean | False                 | Displays or not the final (solved) board                      |
| `--print_check` | Boolean | False                 | Displays the variable check_i: number of uses of the method i |

According to the different settings of the argument the output can vary. When all the argument are put to default, we have the following output:

    Time                   : 0.034s
    estimated difficulty   : 1.29
    number of disjunctions : 0

- `sudokuByConstraints.py`: The main function takes the same two first arguments of the previous function, with the same default values. The function returns the solved table. For the default values, we get the following output:

```
solution:  [
  [3, 7, 2, 1, 5, 6, 4, 9, 8]
  [8, 4, 5, 2, 3, 9, 6, 7, 1]
  [1, 9, 6, 8, 7, 4, 3, 2, 5]
  [5, 3, 1, 6, 2, 7, 8, 4, 9]
  [4, 2, 8, 3, 9, 5, 7, 1, 6]
  [7, 6, 9, 4, 8, 1, 2, 5, 3]
  [9, 1, 4, 7, 6, 3, 5, 8, 2]
  [6, 8, 7, 5, 1, 2, 9, 3, 4]
  [2, 5, 3, 9, 4, 8, 1, 6, 7]
]
```

- `puzzleCreator.py`: The main function takes a minimum difficulty and a minimum number of  cells to be removed and returns such an instance board randomly generated.

